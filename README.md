# Blue Ridge Dog Training Club

## Version 0.1

This is the official WordPress theme for the [Blue Ridge Dog Training Club](http://www.blueridgedogtrainingclub.com/) website.

## Base

This theme is a child theme of the [Genesis Framework](http://StudioPress.com/) from StudioPress.

## Contributors

* Designer: White Spider Design
* Developer: Ten-321 Enterprises
