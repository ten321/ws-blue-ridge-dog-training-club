<?php
/**
 * The main function definitions for the Blue Ridge Dog Training Center theme
 * @version 0.1
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You do not have permission to access this file directly.' );
}

if ( ! class_exists( 'Blue_Ridge_Dog_Training_Club' ) ) {
	class Blue_Ridge_Dog_Training_Club {
		public $version = '0.1.4';

		/**
		 * Construct the class object
		 */
		function __construct() {
			add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
			add_theme_support( 'genesis-responsive-viewport' );
			add_theme_support( 'genesis-structural-wraps', array(
				'header',
				'nav',
				'subnav',
				'site-inner',
				'footer-widgets',
				'footer'
			) );

			//* Add support for custom header
			add_theme_support( 'custom-header', array(
				'default-image'   => get_stylesheet_directory_uri() . '/images/BRDTC_Web_Logo.png',
				'default-color'   => 'ffffff',
				'header_image'    => '',
				'header-selector' => '.site-title a',
				'header-text'     => false,
				'height'          => 138,
				'width'           => 184,
			) );
			remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
			add_action( 'genesis_setup', array( $this, 'register_sidebars' ) );
			add_action( 'after_setup_theme', array( $this, 'adjust_genesis' ) );
			add_action( 'template_redirect', array( $this, 'template_redirect' ) );

			add_filter( 'home_url', array( $this, 'members_to_root_site_title_link' ) );
		}

		function members_to_root_site_title_link( $url ) {
			if ( ! is_multisite() ) {
				return $url;
			}

			if ( ! doing_action( 'genesis_site_title' ) ) {
				return $url;
			}
			if ( is_admin() ) {
				return $url;
			}

			return network_home_url();
		}

		/**
		 * Perform any page-specific changes/actions
		 */
		function template_redirect() {
			if ( is_front_page() ) {
				/*remove_all_actions( 'genesis_loop' );
				add_action( 'genesis_loop', array( $this, 'do_front_page' ) );*/
				remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
				add_action( 'genesis_sidebar', array( $this, 'do_front_page_sidebar' ) );
				add_action( 'genesis_before_content', array( $this, 'do_front_page_tagline' ) );
			}
		}

		/**
		 * Output the White Spider information in the footer
		 */
		function do_copyright_notice() {
			printf( __( '<p class="white-spider">Design by <a href="%s">White Spider</a></p>' ), 'http://whitespiderinc.com/' );
		}

		/**
		 * Set up the style sheets used in this theme
		 */
		function enqueue_styles() {
			wp_register_style( 'genesis', get_stylesheet_uri(), array(), $this->version, 'all' );
			wp_enqueue_style( 'brdtc', get_bloginfo( 'stylesheet_directory' ) . '/brdtc.css', array( 'genesis' ), $this->version, 'all' );
		}

		/**
		 * Register any extra sidebars that will be used in this theme
		 */
		function register_sidebars() {
			genesis_register_sidebar( array( 'id'          => 'before-header',
			                                 'name'        => __( 'Before Header - Left' ),
			                                 'description' => __( 'Appears at the very top of the page, above the main header area, on the left side' )
			) );
			genesis_register_sidebar( array( 'id'          => 'before-header-right',
			                                 'name'        => __( 'Before Header - Right' ),
			                                 'description' => __( 'Appears at the very top of the page, above the main header area, on the right side' )
			) );
			genesis_register_sidebar( array( 'id'          => 'home-features',
			                                 'name'        => __( '[Front Page] Featured Items' ),
			                                 'description' => __( 'Each widget in this area will appear one-third width across the front page, below the main home page content' )
			) );
		}

		/**
		 * Make any adjustments that need to be made to the way Genesis behaves
		 */
		function adjust_genesis() {
			add_action( 'genesis_before_header', array( $this, 'do_before_header' ), 9 );
			add_theme_support( 'genesis-footer-widgets', 1 );
			remove_action( 'genesis_footer', 'genesis_do_footer' );
			remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );
			add_action( 'genesis_footer', 'genesis_footer_widget_areas' );

			add_image_size( 'home-feature', 280, 200, true );
			add_filter( 'image_size_names_choose', array( $this, 'extra_image_sizes' ) );

			$this->unregister_layouts();
		}

		/**
		 * Unregister any custom Genesis page layouts that we won't be using
		 */
		function unregister_layouts() {
			genesis_set_default_layout( 'content-sidebar' );

			$layouts = array(
				'sidebar-content',
				'content-sidebar-sidebar',
				'sidebar-sidebar-content',
				'sidebar-content-sidebar',
			);

			foreach ( $layouts as $l ) {
				genesis_unregister_layout( $l );
			}

			/*remove_theme_support( 'genesis-inpost-layouts' );*/
		}

		/**
		 * Add any necessary image sizes to the choices for inserting an image
		 */
		function extra_image_sizes( $sizes = array() ) {
			return array_merge( $sizes, array( 'home-feature' => __( 'Home Page Feature Item' ) ) );
		}

		/**
		 * Output the front page sidebar
		 */
		function do_front_page_sidebar() {
			dynamic_sidebar( 'home-features' );
		}

		/**
		 * Output the tagline at the top of the home page
		 */
		function do_front_page_tagline() {
			?>
            <section class="home-tagline">
				<?php bloginfo( 'description' ) ?>
            </section>
			<?php
		}

		/**
		 * Output the front page layout
		 * No longer used
		 */
		function do_front_page() {
			return;
			?>
            <section class="home-main">
            <div class="home-tagline">
            </div>
            <article class="home-content">
				<?php
				if ( have_posts() ) : while ( have_posts() ) : the_post();
					?>
                    <header class="entry-header">
                        <h1 class="hidden"><?php bloginfo( 'name' ) ?></h1>
                    </header>
					<?php the_content(); ?>
				<?php
				endwhile; endif;
				?>
            </article>
			<?php
			if ( is_active_sidebar( 'home-features' ) ) {
				?>
                <div class="home-features clearfix">
					<?php dynamic_sidebar( 'home-features' ) ?>
                </div>
                </section>
				<?php
			}
		}

		/**
		 * Output the Before Header widget area
		 */
		function do_before_header() {
			if ( ! is_active_sidebar( 'before-header' ) && ! is_active_sidebar( 'before-header-right' ) ) {
				return;
			}

			?>
            <aside class="before-header widget-area">
                <div class="wrap">
                    <section class="before-header-left">
						<?php dynamic_sidebar( 'before-header' ) ?>
                    </section>
                    <section class="before-header-right">
						<?php dynamic_sidebar( 'before-header-right' ) ?>
                    </section>
                </div>
            </aside>
			<?php
		}
	}
}

global $brdtc_obj;
$brdtc_obj = new Blue_Ridge_Dog_Training_Club;